import VueRouter from "vue-router";
import App from "@/views/App.vue";
import Movies from "@/views/Movies.vue";
import HomePage from "@/views/Home.vue";

const routes = [
    {
        path: "/", component: HomePage

    },
    {
        path: "/app", component: App, name: "App"

    },
    {
        path: "/movies", component: Movies, name: "Movies"
    },

];
const router = new VueRouter({
    routes
});
export default router;