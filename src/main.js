import Vue from 'vue'

import DefaultPage from "./default/defaultPage";
import router from "./router";
import './assets/tailwind.css';
import VueRouter from "vue-router";
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import VueApollo from 'vue-apollo';

Vue.use(VueRouter);

Vue.use(VueApollo);

// HTTP connection to the API
const httpLink = createHttpLink({
  // You should use an absolute URL here
  uri: 'http://localhost:8000/graphql',
})

// Cache implementation
const cache = new InMemoryCache()

// Create the apollo client
const apolloClient = new ApolloClient({
  link: httpLink,
  cache,
});
const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
});

Vue.config.productionTip = false
console.log("apollo", apolloProvider);
Vue.use(router);
new Vue({
  render: h => h(DefaultPage),
  apolloProvider,
  router
}).$mount('#app')
