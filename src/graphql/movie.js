import gql from "graphql-tag";

export const movies = gql`
  query events{
      movies{
        id
        name
        genere
        language
        rating
        price
      }
}`;


export const createMovie = gql`
mutation($input:MovieInput!){
  createMovie(input:$input){
    id
    name
    language
  }
}
`;